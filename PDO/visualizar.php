<?php 
    include("Includes/Header.php"); 
    include("Class/ClassCrud.php");
?>

<div class="content">
    <?php 
        $crud = new ClassCrud();
        $userId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
        $beforeFetch = $crud->select("*", "cadastro", "where id=?", array($userId));
        $fetch = $beforeFetch->fetch(PDO::FETCH_ASSOC);
    ?>
    
    <h1>Dados do Usuário</h1>
    <hr>
    <strong>Nome: </strong><?php echo $fetch['nome']; ?> <br>
    <strong>Cidade: </strong><?php echo $fetch['cidade']; ?> <br>
    <strong>Sexo: </strong><?php echo $fetch['sexo']; ?> <br>
</div>

<?php include("Includes/Footer.php"); ?>