<?php
include("../Includes/Variaveis.php");
include("../Class/ClassCrud.php");

$crud = new ClassCrud();

if ($acao == "Cadastrar") {
    $crud->insert("cadastro", "?,?,?,?", array($id, $nome, $sexo, $cidade));
    echo "Cadastro realizado com Sucesso";
} else {
    $crud->update("cadastro", "nome=?, sexo=?, cidade=?","id=?", array($nome, $sexo, $cidade, $id));
    echo "Cadastro editado com Sucesso";
}