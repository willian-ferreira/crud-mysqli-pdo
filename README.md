# CRUD - MYSQLI/PDO

## Exemplo básico de um CRUD utilizando MYSQLI E PDO

Nesse exemplo de crud é criada uma classe com as funcoes de "INSERT, SELECT, UPDATE e DELETE" de 
maneira que podem ser reutilizadas, apenas com passagem de parâmetros para cada uma dessas quatro funcões, 
assim obtendo mais reaproveitamento de código.