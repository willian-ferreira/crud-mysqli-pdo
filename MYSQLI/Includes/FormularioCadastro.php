<?php 

    include("Class/ClassCrud.php");
    if (isset($_GET['id'])) {
        $acao = "Edicao";
        echo $acao;
        $crud = new ClassCrud();
        $select = $crud->select("*", "cadastro", "where id=?", "i", array($_GET['id']));
        $resultados = $select->fetch_all();
        foreach ($resultados as $resultado) {
            $id = $resultado[0];
            $nome = $resultado[1];
            $sexo = $resultado[2];
            $cidade = $resultado[3];
        }
    } else {
        $acao = "Cadastrar";
        $nome = "";
        $sexo = "";
        $cidade = "";
        $id = 0;
    }
?>

<div class="resultado">
        
</div>

<div class="formulario">
    <h1 class="center">Cadastro</h1>

    <form name="formCadastro" id="formCadastro" method="post" action="Controllers/ControllerCadastro.php">

        <input type="hidden" name="acao" id="acao" value="<?php echo $acao; ?>">
        <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">

        <div class="formulario-input">
            Nome: <br>
            <input type="text" id="nome" name="nome" value="<?php echo $nome; ?>">
        </div>

        <div class="formulario-input">
            Sexo: <br>
            <select name="sexo" id="sexo">
                <option value=""
                <?php  if($sexo == "") { echo "selected"; } ?> >Escolha</option>
                <option value="Masculino" 
                <?php  if($sexo == "Masculino") { echo "selected"; } ?> >Masculino
                </option>
                <option value="Feminino"
                <?php  if($sexo == "Feminino") { echo "selected"; } ?> >Feminino
                </option>
            </select>
        </div>

        <div class="formulario-input">
            Cidade: <br>
            <input type="text" id="cidade" name="cidade" value="<?php echo $cidade; ?>">
        </div>
        
        <div class="formulario-input formulario-input100 center">
            <input type="submit" 
            <?php 
            if($acao == "Edicao") {
                echo "value='Editar'";
            } else {
                echo "value='Cadastrar'";
            }
            ?>
        </div>
    </form>
</div>