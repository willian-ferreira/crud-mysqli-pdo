<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Class Crud</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="Includes/zepto.min.js"></script>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

    <div class="banner">
        <img src="Imagens/crud-php.jpg" alt="">
    </div>
    
    <div class="nav">
        <li><a href="cadastro.php">Cadastro</a></li>
        <li><a href="selecao.php">Seleção</a></li>
        <li><a href="delete.php">Delete</a></li>
        <li><a href="atualizacao.php">Atualização</a></li>
    </div>