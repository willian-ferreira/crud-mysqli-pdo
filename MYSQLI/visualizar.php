<?php 
    include("Includes/Header.php"); 
    include("Class/ClassCrud.php");
?>

<div class="content">
    <?php 
        $crud = new ClassCrud();
        $userId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
        
        $beforeFetch = $crud->select("*", "cadastro", "where id=?", "i", array($userId));
        $fetch = $beforeFetch->fetch_all();
        foreach ($fetch as $usuario) {
    ?>

    <h1>Dados do Usuário</h1>
    <hr>
    <strong>Nome: </strong><?php echo $usuario[1]; ?> <br>
    <strong>Cidade: </strong><?php echo $usuario[2]; ?> <br>
    <strong>Sexo: </strong><?php echo $usuario[3]; ?> <br>

    <?php } ?>
</div>

<?php include("Includes/Footer.php"); ?>