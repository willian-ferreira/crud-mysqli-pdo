<?php
include("../Includes/Variaveis.php");
include("../Class/ClassCrud.php");
$crud = new ClassCrud();

// isss = Inteiro e String, String, String
if ($acao == "Cadastrar") {
    $crud->insert("cadastro", "?,?,?,?", "isss", array($id, $nome, $sexo, $cidade));
    echo "Cadastro realizado com Sucesso";
} else {
    $crud->update("cadastro", "nome=?, sexo=?, cidade=?", "id=?", "sssi", array($nome, $sexo, $cidade, $id));
    echo "Cadastro editado com Sucesso";
}